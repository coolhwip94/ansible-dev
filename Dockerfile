FROM ubuntu

RUN apt update
RUN apt upgrade -y
RUN apt install openssh-server -y
RUN apt install vim -y
RUN service ssh start
RUN apt install pip -y
RUN pip install ansible
