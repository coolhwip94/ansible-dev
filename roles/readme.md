# Roles

- Roles are a level of abstraction on top of tasks and playbooks that let you structure your Ansible configuration in a modular and reusable format


---
## Creating a new role
- Ansible-galaxy provides a quick command to do so
  ```
  ansible-galaxy init <role-name>
  ```
- The example already in this repo is the apache role
---
## Role Directories

- `defaults` : directory to set default variables for independent roles. (can be overwritten in playbooks or inventory files)
- `files`: static files and script that would need to be copied onto a remote server. (ex: index.html, python scripts)
- `meta`: reserved for metadata, where you can define dependencies
- `templates`: templates that will be used for generating files on remote hosts.
- `tasks`: contains files with tasks, the tasks written within these files can reference files directorly without need for directory.
- `vars`: variables for a role can be placed in here and referenced elsewhere
- `tests`: created by default to include example inventory and playbook that calls the created role with localhost as the host
  
- `handlers`: operations that can be run on change

---
>Note: All of these use a main.yml but you can always include extra files 
by adding

```
- include: roles/apache/<directory>/<.yml file>
```
example : 
```
...
tasks:
- include: roles/apache/tasks/tls.yml
```
---
Using a `Role` in a playbook
>example : `playbook.yml` (this is the full playbook, very short and simple)

```
---
- hosts: dev_servers
  become: true
  roles:
    - apache
  vars:
    - doc_root: /var/www/example
```

---
### Resources
- https://www.digitalocean.com/community/tutorials/how-to-use-ansible-roles-to-abstract-your-infrastructure-environment
- https://galaxy.ansible.com/docs/contributing/creating_role.html
- https://docs.ansible.com/