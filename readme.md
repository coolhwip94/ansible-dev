# Ansible Notes and Examples

## Playbooks
This directory will host all playbooks.

---
## Roles

This directory will host all roles, a readme inside this directory will cover the basics of creating a new ansible role.

---

## Main directory (this directory)
- group_vars (holds variables per host group)
- Dockerfile (instructions for docker image)
- docker-compose.yml (spins up 3 vms, ansible-host, server1, and server2)
- ansible.cfg (config file for ansible defaults)

---
## Environment Setup
> This part is **`Optional`** : You can setup an environment however you'd like, below is what the examples in this project are written to use.


### Docker
- Dockerfile and docker-compose.yml have been provided to help in creating your environment.
- The desired state of your environment should be:
  - ubuntu os (any other linux distro should work, just tweak the playbooks)
  - `ansible-dev` user created on each host, with sudo permissions
  - ssh enabled on port 22
- `docker-compose up -d` to get started.
- all hosts should have ansible installed, ansible-host should be used to run plays from.
> It is not required for all hosts to have ansible installed, this docker-compose.yml happens to use the same dockerfile for all environments.

### Helpful setup commands
---
Opening up port for ssh
```
vim /etc/ssh/sshd_config

uncomment port 22

service ssh restart
```

Creating ansible-dev user
```
adduser ansible-dev
password can just be password
```

Creating SSH key
```
ssh-keygen
```

Might be helpful to install sudo
```
apt install sudo -y
```
---