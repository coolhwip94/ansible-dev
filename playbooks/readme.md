# Ansible Playbooks

Ansible Playbooks offer a repeatable, re-usable, simple configuration management and multi-machine deployment system, one that is well suited to deploying complex applications

For more information regarding playbooks visit [here](https://docs.ansible.com/ansible/latest/user_guide/playbooks_intro.html).


## Example playbooks
---
---

### `apache_roles_playbook.yml`
---
- example utilizing a role
- showcases a simple playbook since everything is defined in role

### `check_os.yml`
---
- simple example running a command remotely

### `create-user.yml`
---
- create users using ansible built in module

### `package_installer.yml`
---
- install packages using ansible built in module



## Example output
---
---
**Example output of `check_os.yml` playbook**

```
PLAY [Check Operating System] ********************************************************************************************************************************************************

TASK [Gathering Facts] ***************************************************************************************************************************************************************
ok: [server1]
ok: [server2]

TASK [Read OS file] ******************************************************************************************************************************************************************
changed: [server1]
changed: [server2]

TASK [debug] *************************************************************************************************************************************************************************
ok: [server1] => {
    "output.stdout_lines": [
        "NAME=\"Ubuntu\"",
        "VERSION=\"20.04.3 LTS (Focal Fossa)\"",
        "ID=ubuntu",
        "ID_LIKE=debian",
        "PRETTY_NAME=\"Ubuntu 20.04.3 LTS\"",
        "VERSION_ID=\"20.04\"",
        "HOME_URL=\"https://www.ubuntu.com/\"",
        "SUPPORT_URL=\"https://help.ubuntu.com/\"",
        "BUG_REPORT_URL=\"https://bugs.launchpad.net/ubuntu/\"",
        "PRIVACY_POLICY_URL=\"https://www.ubuntu.com/legal/terms-and-policies/privacy-policy\"",
        "VERSION_CODENAME=focal",
        "UBUNTU_CODENAME=focal"
    ]
}
ok: [server2] => {
    "output.stdout_lines": [
        "NAME=\"Ubuntu\"",
        "VERSION=\"20.04.3 LTS (Focal Fossa)\"",
        "ID=ubuntu",
        "ID_LIKE=debian",
        "PRETTY_NAME=\"Ubuntu 20.04.3 LTS\"",
        "VERSION_ID=\"20.04\"",
        "HOME_URL=\"https://www.ubuntu.com/\"",
        "SUPPORT_URL=\"https://help.ubuntu.com/\"",
        "BUG_REPORT_URL=\"https://bugs.launchpad.net/ubuntu/\"",
        "PRIVACY_POLICY_URL=\"https://www.ubuntu.com/legal/terms-and-policies/privacy-policy\"",
        "VERSION_CODENAME=focal",
        "UBUNTU_CODENAME=focal"
    ]
}

PLAY RECAP ***************************************************************************************************************************************************************************
server1                    : ok=3    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
server2                    : ok=3    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
```


### References
---
---
- https://docs.ansible.com/ansible/latest/user_guide/playbooks_intro.html