# Using Ansible Vault

## Using file to encrypt string
---
This uses the `vault/example_file.txt` to encrypt a string `'hello_world'` with a name `'test_name'` that can be referenced later.

```
ansible-vault encrypt_string --vault-password-file vault/example_file.txt 'hello_world' --name 'test_name'
```
### Example output below : 
>(add this to `group_vars/credentials.yml` or any other variable file)
```
test_name: !vault |
          $ANSIBLE_VAULT;1.1;AES256
          63393765656131656330656532316266343261303662613038353562616431643636343339346263
          6438323932633739313531326437313135373232383363350a393564383735346435366433343736
          65323131303061613831383562363863386431326462656561666233343963323039313464313833
          3538653063313938390a313231323235313234653763386131336162343264653664636562393532
          3937
```
### Test it out
---
#### Commands
>These are ansible adhoc commands, basically just sending a shell command through
>- Notice how the variable name is able to be used within our command

```
ansible <hosts> -a "echo {{<variable_name>}}"

example : 

ansible dev_servers -a "echo {{test_name}}"
```
#### Response
```
server1 | CHANGED | rc=0 >>
hello world
server2 | CHANGED | rc=0 >>
hello world
```

---

### References
- https://docs.ansible.com/ansible/latest/user_guide/vault.html
- https://docs.ansible.com/ansible/latest/reference_appendices/config.html