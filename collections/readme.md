# Ansible Collections

Collections are a distribution format for Ansible content that can include playbooks, roles, modules, and plugins. As modules move from the core Ansible repository into collections, the module documentation will move to the collections pages.

> You can install and use collections through Ansible Galaxy.

---
### Finding ansible collection
- https://galaxy.ansible.com/


### Install directory
- Ansible collections will be installed to the `collections` directory as set in the `ansible.cfg` file.
  
---

## Installing an ansible collection
```
ansible-galaxy collection install <collection_name>
```
Example :
```
ansible-galaxy collection install ciena.mdso
```

---


## Using Collection in playbook
```
- hosts: all
  tasks:
    - my_namespace.my_collection.mymodule:
        option1: value

```
---

## Helpful Commands

- Viewing installed collections
  ```
  ansible-galaxy collection list
  ```
- Verify collections
  ```
  ansible-galaxy collection verify my_namespace.my_collection
  
  example:
  ansible-galaxy collection verify ciena.mdso
  ```

---
References:
- https://docs.ansible.com/ansible/latest/user_guide/collections_using.html